package ru.will0376.fixloader;

import io.github.crucible.omniconfig.api.OmniconfigAPI;
import io.github.crucible.omniconfig.gconfig.AnnotationConfigCore;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.ICrashCallable;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.discovery.ASMDataTable;
import net.minecraftforge.fml.common.discovery.asm.ModAnnotation;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import org.apache.logging.log4j.Logger;
import ru.will0376.fixloader.command.FixLoaderCommand;
import ru.will0376.fixloader.utils.FixInfo;
import ru.will0376.fixloader.utils.FixLoad;

import java.text.SimpleDateFormat;
import java.util.Set;

@Mod(modid = FixLoader.MOD_ID, name = FixLoader.MOD_NAME, version = FixLoader.VERSION)
public class FixLoader {
	public static final String MOD_ID = "fixloader";
	public static final String MOD_NAME = "FixLoader";
	public static final String VERSION = "1.0";
	public static final String BUILD_HASH = "@Hash@";
	@Deprecated
	public static final boolean remap = false;
	@Mod.Instance(MOD_ID)
	public static FixLoader INSTANCE;
	private Logger modLog;
	private Set<ASMDataTable.ASMData> fixAnnotationClasses;


	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) throws Exception {
		registerCrashHandler();
		modLog = event.getModLog();
		fixAnnotationClasses = event.getAsmData().getAll("ru.will0376.fixloader.utils.identifiers.Fix");
		if (!FixCoreLoader.skipLoading)
			for (ASMDataTable.ASMData asmData : fixAnnotationClasses) {
				if (asmData.getAnnotationInfo().containsKey("type")) {
					ModAnnotation.EnumHolder type = (ModAnnotation.EnumHolder) asmData.getAnnotationInfo().get("type");
					String fullClassName = asmData.getClassName();
					String substring = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
					switch (type.getValue()) {
						case "Event":
							event.getModLog().info("Loading event: " + substring);
							MinecraftForge.EVENT_BUS.register(Class.forName(asmData.getClassName()).newInstance());
							break;

						case "Config":
							event.getModLog().info("Loading config: " + substring);
							Class<?> annotationConfigClass = Class.forName(asmData.getClassName());
							if (!AnnotationConfigCore.INSTANCE.getAssociatedOmniconfig(annotationConfigClass).isPresent())
								OmniconfigAPI.registerAnnotationConfig(annotationConfigClass);
							break;
					}
				}
			}
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		modLog.info("Loading FixLoader Hash: " + BUILD_HASH);
	}

	@Mod.EventHandler
	public void event(FMLServerStartingEvent event) {
		event.registerServerCommand(new FixLoaderCommand());
		int errorCount = FixLoad.getErrorCount();
		if (errorCount >= 1)
			modLog.error(String.format("%s [FixLoader] There were %s errors loading the fixes", TextFormatting.RED, errorCount));
	}

	public Set<ASMDataTable.ASMData> getFixAnnotationClasses() {
		return fixAnnotationClasses;
	}

	private void registerCrashHandler() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
		FMLCommonHandler.instance().registerCrashCallable(new ICrashCallable() {
			@Override
			public String call() {
				StringBuilder builder = new StringBuilder();
				for (FixInfo info : FixLoad.getInfos()) {
					builder.append("\n")
							.append("[")
							.append(info.getMod())
							.append("] ")
							.append(info.getFixname())
							.append("-")
							.append(info.getBuildVersion())
							.append(" -> ")
							.append(simpleDateFormat.format(info.getCompileTime()));
				}

				return builder.append("\n").toString();
			}

			@Override
			public String getLabel() {
				return "\nLoaded fixes [Modid, Fixname-BuildVersion, BuildTime]";
			}
		});
	}
}
