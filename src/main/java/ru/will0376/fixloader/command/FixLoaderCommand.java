package ru.will0376.fixloader.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import ru.will0376.fixloader.utils.FixInfo;
import ru.will0376.fixloader.utils.FixLoad;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FixLoaderCommand extends CommandBase {
	private static final Map<String, Boolean> crutches = new HashMap<>();
	private static boolean crutchesEnable = false;

	public static boolean getCrutch(String name) {
		if (!crutchesEnable)
			return false;
		if (!crutches.containsKey(name)) {
			crutches.put(name, false);
			return false;
		}
		return crutches.get(name);
	}

	@Override
	public String getName() {
		return "fixloader";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (sender.canUseCommand(4, "fixloader")) {
			if (args.length >= 1) {
				switch (args[0]) {
					case "crutch":
						if (args.length == 2) {
							String arg = args[1];
							if (crutches.containsKey(arg)) {
								crutches.remove(arg);
								sender.sendMessage(new TextComponentString("Removed!"));
							} else {
								crutches.put(arg, true);
								sender.sendMessage(new TextComponentString("Added!"));
							}
						}
						break;

					case "crutchlist":
						sender.sendMessage(new TextComponentString("From List: "));
						crutches.forEach((s, b) -> sender.sendMessage(new TextComponentString(s + "--> " + b)));
						break;

					case "dumpFixLoaderList":
						for (FixInfo info : FixLoad.getInfos()) {
							System.out.println(info.toString());
						}
						break;

					case "dumpFixNames":
						for (FixInfo info : FixLoad.getInfos()) {
							System.out.println(info.getFixname() + " " + info.getFixLoaderCommitCounter());
						}
						break;

					case "togglecrutches":
						crutchesEnable = !crutchesEnable;
						sender.sendMessage(new TextComponentString("Done! New value: " + crutchesEnable));
						break;
				}
			}
		}
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		if (args.length >= 1 && args[0].equals("crutch")) {
			return getListOfStringsMatchingLastWord(args, crutches.keySet());
		}
		return getListOfStringsMatchingLastWord(args, "crutch", "crutchlist", "dumpFixLoaderList", "togglecrutches");
	}
}
