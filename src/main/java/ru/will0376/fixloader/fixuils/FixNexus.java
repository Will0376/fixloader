package ru.will0376.fixloader.fixuils;

import com.gamerforea.eventhelper.nexus.ModNexus;
import com.gamerforea.eventhelper.nexus.ModNexusFactory;
import com.gamerforea.eventhelper.nexus.NexusUtils;
import ru.justagod.cutter.GradleSide;
import ru.justagod.cutter.GradleSideOnly;

@ModNexus(name = "FixLoader")
@GradleSideOnly(GradleSide.SERVER)
public class FixNexus {
	public static final ModNexusFactory NEXUS_FACTORY = NexusUtils.getFactory();
}
