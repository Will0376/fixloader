package ru.will0376.fixloader.utils;

import io.github.crucible.grimoire.common.api.GrimoireAPI;
import lombok.extern.log4j.Log4j2;
import net.minecraftforge.fml.common.FMLCommonHandler;
import ru.will0376.fixloader.utils.identifiers.FixIdentifierWrapper;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class ClassUtils {
	public static void checkClasses() {
		for (FixInfo info : FixLoad.getInfos()) {
			List<FixIdentifierWrapper> removedIds = new ArrayList<>();
			if (GrimoireAPI.getMinecraftVersion().equals(info.getMcVersion())) {
				for (FixIdentifierWrapper id : info.getWrapperList()) {
					if (id.getSide().checkSide()) {
						log.info("Passed class: {}", id.getFixClassName());
					} else {
						FixLoad.incrementError();
						log.error("Loaded class {} for another side of minecraft ({})", id.getFixClassName(), id.getSide()
								.name());
						removedIds.add(id);
					}
				}
			} else {
				log.error("Loaded fix {} for another version of minecraft ({})", info.getFixname(), info.getMcVersion());
				FMLCommonHandler.instance().exitJava(1, false);
			}
			info.getWrapperList().removeAll(removedIds);
		}
	}

	public static File tryGetParentFolder(File file) {
		return new File(file.getAbsolutePath()).getParentFile();
	}

	public static URLClassLoader loadClassesFromCompiledDirectory(ClassLoader parent, File classesDir) throws Exception {
		return new URLClassLoader(new URL[]{classesDir.toURI().toURL()}, parent);
	}
}
